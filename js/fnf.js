var fnf = (function(){

  /* ---- Variables ---- */
  var frames = 440,
    stops = [
      {
        name: 'Overview',
        startframe: 30
      }, {
        name: 'Design',
        startframe: 70
      }, {
        name: 'Versatility',
        startframe: 105
      }, {
        name: 'Wireless Charging',
        startframe: 135,
        parttwo: 163
      }, {
        name: 'Communications',
        startframe: 210,
        parttwo: 253
      }, {
        name: 'Modules',
        startframe: 288
      }, {
        name: 'Security',
        startframe: 339
      }, {
        name: 'Desktop',
        startframe: 379
      }, {
        name: 'Meeting',
        startframe: 404
      }, {
        name: 'Finish',
        startframe: 440
      }
    ],
    zooms =   [
      {
        name: 'Design',
        slug: 'design',
        framesMid: 100,
        frames: 145,
        tempo: .12,
        copyClass: '1-1',
        openDelay: 5000,
        closeDelay: 3000,
        pause: false,
      },
      {
        name: 'Modules',
        slug: 'modules',
        frameStops: [
          {openDelay: 0, closeDelay: 2000},
          {openDelay: 2200, closeDelay: 4000},
          {openDelay: 4200, closeDelay: 6000},
          {openDelay: 6200, closeDelay: 8000}
        ],
        frames: 320,
        tempo: .12,
        copyClass: '5',
        pause: false
      },
      {
        name: 'Security',
        slug: 'security',
        framesMid: 78,
        frames: 150,
        tempo: .125,
        copyClass: '6-1',
        openDelay: 2500,
        closeDelay: 4500,
        pause: false
      }
    ],
    $cont = $('.fnf-int-mod-cont'),
    $contbg = $('.fnf-opened-bg'),
    $stage = $('.stage'),
    $image = $('#image'),
    $window = $(window),
    preload = 0;


  /* ---- Initialization ---- */
  function init() {
      _applyEventListeners('desktop');
  }


  /* ---- Event Listeners ---- */
  function _applyEventListeners() {

    /* -- Universal Events -- */
    $cont.on('click', '.zoom', function() {
      var zoom = $(this).data('zoom');
      _loadZoom(zoom);
    });

      /* -- Viewport Resize -- */
    $window.on('resize', _throttle(function() {
      if (_isInViewport($cont) &&_isOpened($cont)) {
        _centerModule();
      }
    }, 200));

    $cont.on('click', '.fnf-overlay-trig', function(){ _openModule('desktop'); });
    $cont.on('click', '.close-overlay', function(){ _closeModule('desktop'); });

    /* -- First Slide Button -- */
    $cont.on('click', '.onward', function() {
      var stop = $(this).data('next-stop');
      _gotoStop(stop);
    });

  }


  /* ---- Animation Reel Methods ---- */
  /* -- Main Reel Invocation -- */
  function _invokeMainReel(frame) {
    $image.reel({
        wheelable: true,
        scrollable: true,
        draggable: false,
        throwable: false,
        timeout: 0,
        tempo: 12,
        frames: frames,
        frame: 0,
        loops: false,
        revolution: 10000,
        vertical: true,
        horizontal: false,
        delay: 9999,
        path: 'img/main/',
        laziness: 1,
        preload: 'linear'
      })
      .bind('loaded', function (ev) {
        return false;
      })
      .bind('preloaded', function (ev) {
        if (preload === stops[0].startframe) {
          setTimeout(function(){
            _gotoStop(0);
          },0);
        }
        preload++
      })
      .bind('frameChange', function(event, nil, frame) {
        if ($stage.attr('data-active') == 'true' ) {
          _hideCopy();
        }
      })
      .bind('wheel', function(ev) {
        if ($stage.attr('data-active') === 'false') {
          var stop = parseInt($stage.attr('data-stop')),
            deltaY = ev.originalEvent.deltaY;
          if (deltaY > 15) {
            if (stop < 9) {
              _gotoStop(stop + 1);
            }
          } else if (deltaY <= -40) {
            if (stop > 0) {
              _gotoStop(stop - 1);
            }
          }
        }
        ev.preventDefault();
        ev.stopPropagation();
      })
      .bind('stop', function (event, nil, frame) {
        $stage.attr('data-active', 'false');
        var activeStop = _getActiveStop('desktop');

        if(_isOpened($cont)) {
          var stop = $stage.attr('data-stop');
          _showCopy(stop);
          $('.buttons').removeClass('ghost');
          $('.no-scroll').addClass('ghost');
        }

        if (activeStop == 3){
          setTimeout(function() {
            _gotoFrame(stops[3].parttwo);
          }, 1500)
        }

        if (activeStop == 4){
          setTimeout(function() {
            _gotoFrame(stops[4].parttwo);
          }, 1500)
        }

      })
      .hammer().bind("swipeup swipedown panup pandown", function(ev) {
      if ($stage.attr('data-active') === 'false') {
        var stop = parseInt($stage.attr('data-stop')),
          deltaY = ev.gesture.deltaY;
        if (deltaY > 10) {
          if (stop < 9) {
            _gotoStop(stop + 1);
          }
        } else if (deltaY <= -10) {
          if (stop > 0) {
            _gotoStop(stop - 1);
          }
        }
      }
    });
  }

  /* -- Zoom Reel Invocation -- */
  function _invokeZoomReel(z, zoomSlug) {
    _hideCopy();

    var frames = zooms[z].frames - 1,
      zoomID = '#' + zoomSlug,
      $zoomID = $(zoomID);

    $zoomID.reel({
        wheelable: false,
        scrollable: false,
        draggable: false,
        throwable: false,
        cursor: 'default',
        timeout: 0,
        tempo: 12,
        frames: frames,
        frame: 0,
        loops: false,
        vertical: true,
        horizontal: false,
        delay: 9999,
        path: 'img/' + zoomSlug + '/',
        laziness: 1
      })
      .bind('loaded', function (ev) {

        setTimeout(function() {
          if (zooms[z].pause) {
            _gotoFrame(zooms[z].framesMid -1, zoomID, zooms[z].tempo);
            $('.copy-' + zooms[z].copyClass).removeClass('ghost');
            setTimeout(function () {
              $('.copy-' + zooms[z].copyClass).find('.anim').removeClass('ghost');
              setTimeout(function () {
                $('.copy-' + zooms[z].copyClass).find('.anim').addClass('ghost');
                _gotoFrame(frames, zoomID, zooms[z].tempo);
              }, zooms[z].closeDelay);
            }, zooms[z].openDelay);
          } else {
            _gotoFrame(frames, zoomID, zooms[z].tempo);

            if (zooms[z].frameStops){

              for (var i = 0; i < zooms[z].frameStops.length; i++ ) {

                var frameData = {
                  $targetCopy: $('.copy-' + zooms[z].copyClass + '-' + (i + 1)),
                  targetDelays: zooms[z].frameStops[i]
                };

                (function(e) {
                  e.$targetCopy.removeClass('ghost');
                  setTimeout(function () {
                    e.$targetCopy.find('.anim').removeClass('ghost');
                  }, e.targetDelays.openDelay);
                  setTimeout(function () {
                    e.$targetCopy.find('.anim').addClass('ghost');
                  }, e.targetDelays.closeDelay);
                })(frameData);

              }

            } else {

              $('.copy-' + zooms[z].copyClass).removeClass('ghost');
              setTimeout(function () {
                $('.copy-' + zooms[z].copyClass).find('.anim').removeClass('ghost');
                setTimeout(function () {
                  $('.copy-' + zooms[z].copyClass).find('.anim').addClass('ghost');
                }, zooms[z].closeDelay);
              }, zooms[z].openDelay);

            }

          }
        },0);

      })
      .bind('stop', function (event, nothing, frame) {
        if (frames === $zoomID.data('frame')) {
          _unloadZoom();
        }
      });
  }

  /* ---- Visibility Methods ---- */
  /* -- Is Element Open -- */
  function _isOpened (el) {
    return el.hasClass('opened');
  }

  /* -- Is Element in View -- */
  function _isInViewport (el) {
    var rect = el[0].getBoundingClientRect();
    return (
      rect.top >= -(rect.height*1.5) &&
      rect.top <= (window.innerHeight*1.5)
    );
  }

  /* ---- Button Functions ---- */
  /* -- Set up sidebar buttons -- */
  function _buildButtons() {
    var markup = '';
    for (i = 0; i < stops.length; i++) {
      markup += '<a onclick="fnf.buttonClick(' + i + ');" class="marker marker-'
        + i + '" title="' + stops[i].name + '"><span>' + stops[i].name + '</span></a>';
    }
    $('.buttons').html(markup);
  }

  /* -- Button Click Event -- */
  function buttonClick(stop, viewport) {

      var currentStop = _getActiveStop(viewport),
        delta = stop - currentStop;

      if (Math.abs(delta) === 1) {
        _gotoStop(stop);
      } else if (delta == 0){
        return false;
      } else {
        _jumpStop(stop)
      }

  }

  /* -- Set selected button -- */
  function _makeSelected(stop) {
    _makeUnSelected();
    $('.marker-' + stop).addClass('selected');
  }

  /* -- Unset selected button -- */
  function _makeUnSelected() {
    $('.marker').removeClass('selected');
  }


  /* ---- Animation Methods ---- */
  /* -- Animate to frame -- */
  function _gotoFrame(frame, target, tempo) {
    target = target || '#image';
    tempo = tempo || 0.065;
    //$(target).trigger('stop');
    $(target).trigger('reach', [frame, tempo]);
  }

  /* -- Animate to stop -- */
  function _gotoStop(stop) {
    $stage.attr('data-active', 'true');
    _setActiveStop(stop);
    _gotoFrame(stops[stop].startframe);
  }

  /* -- Go directly to a frame -- */
  function _jumpFrame(frame) {
    $image.trigger('stop');
    $image.reel('frame', frame);
  }

  /* -- Fade directly to a stop -- */
  function _jumpStop(stop) {
    _setActiveStop(stop);
    $stage.attr('data-active', 'true');
    var frame = stops[stop].startframe;

    $('.white-overlay').removeClass('ghost');
    setTimeout(function(){

      _jumpFrame(frame);
      setTimeout(function(){
        $('.white-overlay').addClass('ghost');
        $stage.attr('data-active', 'false');
        _showCopy(stop);
      }, 150);
    }, 300);
  }

  /* -- Get active stop context -- */
  function _getActiveStop(viewport) {
    return $stage.attr('data-stop');
  }

  /* -- Set active stop context -- */
  function _setActiveStop(stop) {
    $stage.attr('data-stop', stop);
  }


  /* ---- Copy Methods ---- */
  /* -- Show a stop's copy -- */
  function _showCopy (stop, zoom) {
    if (!$('.marker-' + stop).hasClass('selected') || zoom){
      _hideCopy();
      _waterfall(stop);
      _makeSelected(stop);
    }
  }

  /* -- Hide all copy -- */
  function _hideCopy (viewport) {
    if (viewport === 'mobile') {
      $('.slide.inner, .slide.inner .anim').addClass('ghost');
    } else {
      $('.copy.inner, .copy.inner .anim').addClass('ghost');
    }
  }

  /* -- Animate copy reveal -- */
  function _waterfall(stop, mobile) {
    if (mobile) {
      var $target = $('.slide-' + (stop));
    } else {
      var $target = $('.copy-' + (stop));
    }
    $target.removeClass('ghost').find('.ghost').each(function(){
      var $this = $(this),
        timeout = $this.data('anim');

      setTimeout(function(){
        $this.removeClass('ghost');
      }, timeout);
    })
  }


  /* ---- Module Methods ---- */
  /* -- Open Module -- */
  function _openModule() {
      _invokeMainReel();
      _hideCopy();
      $cont.addClass('opened');
      $contbg.addClass('opened');
      $('.copy-intro').addClass('ghost');
      $('.close-overlay').removeClass('ghost');
      $('body').addClass('overlay-lock');

    setTimeout(function(){
      _buildButtons();
    },0);
    setTimeout(function(){
      _centerModule();
    },0);
  }

  /* -- Close Module -- */
  function _closeModule() {
    $('.white-overlay').removeClass('ghost');
    $stage.attr('data-stop', 0);
    _unloadZoom();
    $('.copy').addClass('ghost');
    setTimeout(function () {
      $('body').removeClass('overlay-lock');
      $('.close-overlay').addClass('ghost');
      $cont.removeClass('opened');
      $contbg.removeClass('opened');
      $('.buttons').hide();
      $('.copy-intro, .no-scroll').removeClass('ghost');
      $cont.css('margin-top', 0);
      _resetAll(40);
      setTimeout(function () {
        $('.white-overlay').addClass('ghost');
      }, 150);
    }, 300);
  }


  /* -- Load inner zoom reel -- */
  function _loadZoom (z) {
    var zoomSlug = 'zoom-' + zooms[z].slug;
    $('#' + zoomSlug).removeClass('ghost').addClass('opened');
    $('.copy-' + zooms[z].copyClass).removeClass('ghost');
    $('.buttons, .close-overlay').addClass('ghost');
    $('.close-zoom').removeClass('ghost');
    _invokeZoomReel(z, zoomSlug);
  }

  /* -- Unload inner zoom reel -- */
  function _unloadZoom () {
    $('.zoom-reel').each(function(){
      if ($(this).hasClass('opened')){
        $(this).addClass('ghost').removeClass('opened').unreel();
      }
    });
    $('.buttons, .close-overlay').removeClass('ghost');
    $('.close-zoom').addClass('ghost');

    var stop = $stage.attr('data-stop');
    _showCopy(stop, true);
  }

  /* -- Back to square 1 -- */
  function _resetAll(viewport) {
    _makeSelected('0');
    _jumpFrame(30);
    _centerModule();
  }

  function _applyTransform(elem, translate) {
    elem.style.transform = translate;
    elem.style.mozTransform = translate;
    elem.style.webkitTransform = translate;
  }

  /* ---- Alignment Methods ---- */
  /* -- Center Module -- */
  function _centerModule(viewport) {

      var winHeight = window.innerHeight,
        winHalf = (winHeight / 2),
        contHeight = $cont.height(),
        contMiddle = contHeight / 2,
        elementOffsetTop = $cont.offset().top,
        truMid = (elementOffsetTop + contMiddle - winHalf);

    $('html, body').scrollTop(truMid);

  }

  /* ---- Utility Functions ---- */
  function _throttle(fn, threshhold, scope) {
    threshhold || (threshhold = 250);
    var last,
      deferTimer;
    return function () {
      var context = scope || this;

      var now = +new Date,
        args = arguments;
      if (last && now < last + threshhold) {
        // hold on to it
        clearTimeout(deferTimer);
        deferTimer = setTimeout(function () {
          last = now;
          fn.apply(context, args);
        }, threshhold);
      } else {
        last = now;
        fn.apply(context, args);
      }
    };
  }

  function _debounce(func, wait, immediate) {
    var timeout;
    return function() {
      var context = this, args = arguments;
      var later = function() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  }



  /* ---- Exposed Functions ---- */
  return {
    init: init,
    buttonClick: buttonClick
  }
})();

$(function() {
  fnf.init('desktop');
});
