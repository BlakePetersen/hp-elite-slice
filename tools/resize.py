#!/usr/bin/python
from PIL import Image
import glob, os

def resize(dir, pattern):
    for pathAndFilename in glob.iglob(os.path.join(dir,pattern)):

        print pathAndFilename

        if os.path.isfile(pathAndFilename):
            im = Image.open(pathAndFilename)
            f, e = os.path.splitext(pathAndFilename)
            imResize = im.resize((1280,720), Image.ANTIALIAS)
            imResize.save(f + '.jpg', 'JPEG', quality=90)

resize(r'img', r'*.jpg')